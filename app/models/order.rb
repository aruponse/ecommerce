class Order < ApplicationRecord
  has_many :items, class_name: 'OrderItem', :dependent => :delete_all

  enum status: %w{cart open delivered removed}
end
