class OrdersController < ApplicationController
  def index
    @orders = Order.where(status: Order.statuses[:open])
  end

  def new
    @order = current_cart.order
  end

  def items
    @order = Order.find params[:order_id]
    # @items = OrderItem.where order_id: params[:order_id]
  end
  def create
    @order = current_cart.order

    if @order.update_attributes(order_params.merge(status: Order.statuses[:open]))
      session[:cart_token] = nil
      redirect_to root_path
    else
      render :new
    end
  end

  def destroy
    Order.destroy params[:id]
    redirect_to orders_path
  end

  private

  def order_params
    params.require(:order).permit(:first_name, :last_name)
  end
end
