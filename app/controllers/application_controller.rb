class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def current_cart
    @current_cart ||= ShoppingCart.new token: cart_token
  end
  helper_method :current_cart

  def cart_token
    @cart_token unless @cart_token.nil?

    session[:cart_token] ||= SecureRandom.hex 8
    @cart_token = session[:cart_token]
  end
end
