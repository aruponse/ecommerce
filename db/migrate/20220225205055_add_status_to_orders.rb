class AddStatusToOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :orders, :status, :integer, default: 'cart'

    Order.where(status: nil).update_all(status: Order.statuses[:cart])
  end
end
