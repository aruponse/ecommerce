class CreateOrderItems < ActiveRecord::Migration[5.2]
  def change
    create_table :order_items do |t|
      t.integer :quantity
      t.decimal :price
      t.belongs_to :product, foreign_key: true
      t.belongs_to :order, foreign_key: true

      t.timestamps
    end

    add_foreign_key :order_items, :orders, name: 'fk_order_items_to_order'
    add_foreign_key :order_items, :products, name: 'fk_order_items_to_product'
  end
end
