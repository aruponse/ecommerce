Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'products#index'

  resources :products , only: :index
  resources :orders , only: [:index, :destroy] do
    get 'items', to: 'orders#items', as: :products
  end
  resources :order_items, only: [:index, :create, :destroy], path: '/cart/items'

  get '/cart', to: 'order_items#index'
  get 'cart/checkout', to: 'orders#new', as: :checkout
  patch 'cart/checkout', to: 'orders#create'
end
